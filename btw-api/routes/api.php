<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/', function (Request $request) {
    return response(['Hello' => 'World'], 200);
});

Route::post('/login', [App\Http\Controllers\Auth\LoginController::class, 'index']);

Route::middleware('auth:api')->group(function () {
    Route::post('/logout', [App\Http\Controllers\Auth\LogoutController::class, 'index']);
});
